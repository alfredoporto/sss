<?php
class MysqlUser{
  
    // database connection and table name
    private $conn;
    public $code;
    public $pass;
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read products
    function createDatabase(){
    
        // select all query
        $query = "CALL proc_create(?,?)";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        $this->code=htmlspecialchars(strip_tags($this->code));
        $this->pass=htmlspecialchars(strip_tags($this->pass));
        $stmt->bindValue(1, $this->code, PDO::PARAM_STR);
        $stmt->bindValue(2, $this->pass, PDO::PARAM_STR);
    
        // execute query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }
}
?>