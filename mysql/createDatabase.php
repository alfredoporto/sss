<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once './MysqlDatabase.php';
include_once './MysqlUser.php';
include_once '../psql/PsqlDatabase.php';
include_once '../psql/PsqlUser.php';

$data = json_decode(file_get_contents("php://input"));
$databaseType = $data->databaseType;
$operation = $data->operation;
$code = $data->code;
$pass = $data->pass;

switch(true){
    case($databaseType == 'mysql'):
        $database = new MysqlDatabase();
        $db = $database->getConnection();

        $user = new MysqlUser($db);
        $user->code = $code;
        $user->pass = $pass;

        if($user->createDatabase()){http_response_code(200);
            echo json_encode(array("message" => "Creación de databse exitoso"));
        }else{
            http_response_code(503);
            echo json_encode(array("message" => "Ocurrió un error"));
        }

    case($databaseType == 'psql'):
        $database = new PsqlDatabase();
        $db = $database->getConnection();

        $user = new PsqlUser($db);
        $user->code = $code;
        if($user->createSchema()){http_response_code(200);
            echo json_encode(array("message" => "Creación de schema exitoso"));
        }else{
            http_response_code(503);
            echo json_encode(array("message" => "Ocurrió un error"));
        }        
}


