--ejecutar con usuario postgresql
--creacion de funcion para crear usuario y esquema
CREATE OR REPLACE FUNCTION public.create_environment(code_user character varying)
	RETURNS int4 
	/*Base types are those, like int4, that are implemented below the 
	level of the SQL language (typically in a low-level language such as C). 
	They generally correspond to what are often known as abstract data types. 
	PostgreSQL can only operate on such types through functions provided by 
	the user and only understands the behavior of such types to the extent 
	that the user describes them*/
	LANGUAGE plpgsql
AS $function$
DECLARE
	p_return int4 ;
BEGIN
	execute 'create schema '||quote_ident($1) ;
	execute 'create user '||quote_ident($1) ||' password ''' ||quote_ident($1) || '''';
	execute 'grant all on schema '||quote_ident($1) ||' to '||quote_ident($1) ;	
	
	p_return = 0; 
	RETURN p_return;
END;$function$
;

--invocacion a funcion
select public.create_environment('rony');

/*quote_ident is used for identifiers quoting. quote_literal is used for string quoting.*/

CREATE PROCEDURE  proc_create ( IN code_user VARCHAR, IN pass_user VARCHAR)
DELIMITER $$
       BEGIN
	     SET @createdb = CONCAT ("CREATE DATABASE ", code_user);
		 SET @createuser = CONCAT ("CREATE USER ", cod_user, "IDENTIFIED BY ", pass_user);
		 SET @grantuser = CONCAT ("GRANT ALL ON ", code_user " TO ", code_user );

		 PREPARE createdb FROM @createdb;
		 PREPARE createuser FROM @createuser;
		 PREPARE grantuser FROM @grantuser;

		 EXECUTE createdb;
		 EXECUTE createuser;
		 EXECUTE grantuser;

		 DEALLOCATE PREPARE createdb;
		 DEALLOCATE PREPARE createuser;
		 DEALLOCATE PREPARE grantuser;
	   	 
       END//
DELIMITER;
