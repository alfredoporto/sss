<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
// include database and object files
include_once './Database.php';
include_once './User.php';
  
// instantiate database and product object
$database = new PsqlDatabase();
$db = $database->getConnection();
  
// initialize object
$user = new PsqlUser($db);
$data = json_decode(file_get_contents("php://input"));

if(!empty($data->code)){
    $user->code = $data->code; 

    if($user->createSchema()){
        // set response code - 200 OK
        http_response_code(200);
      
        // show products data in json format
        echo json_encode(array("message" => "Creación de schema exitoso"));
    }else{
        http_response_code(503);
  
        // tell the user
        echo json_encode(array("message" => "Ocurrió un error"));
    }
}

  

  
