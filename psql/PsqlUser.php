<?php
class PsqlUser{
  
    // database connection and table name
    private $conn;
    public $code;
  
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read products
    function createSchema(){
    
        // select all query
        $query = "SELECT public.create_environment(?)";
        // prepare query statement
        $stmt = $this->conn->prepare($query);
        $this->code=htmlspecialchars(strip_tags($this->code));
        $stmt->bindValue(1, $this->code, PDO::PARAM_STR);
    
        // execute query
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }
}
?>